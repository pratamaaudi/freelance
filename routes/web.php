<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/demo', 'DemoController@index')->name('demo.index');

Route::get('/demo/category', 'DemoController@showCategory')->name('demo.category');

Route::get('/demo/contact', 'DemoController@showContact')->name('demo.contact');

Route::get('/demo/cart', 'DemoController@showCart')->name('demo.cart');

Route::get('/demo/product', 'DemoController@showProduct')->name('demo.product');